import React, { Component } from 'react'
import { ScrollView, Text, Image, View, Button, FlatList, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import PokemonActions from '../Redux/PokemonRedux' //here
import { withNavigation } from 'react-navigation'

class DetailScreen extends Component {
    constructor(props){
        super(props)
        this.state={
            name:props.navigation.getParam('name'),
            data:[],
            errorMessage: ''
        }
    }

    componentDidUpdate(prevProps){
        if(prevProps.pokemonDetailResponse !== this.props.pokemonDetailResponse){
            console.log('componentDidUpdate',this.props.pokemonDetailResponse)
            this.setState({data: this.props.pokemonDetailResponse})
        }

        if(prevProps.pokemonDetailError !== this.props.pokemonDetailError){
            this.setState({errorMessage: this.props.pokemonDetailError})
        }
    }

    componentDidMount(){
        let param = {name:this.state.name}
        this.props.pokemonDetailRequest(param)
    }

    render () {
        const {data} = this.state;
        console.log(this.state.name)
        return (
            <View style={{flex: 1, alignItems: 'center'}}>
                <Image
                    style={{height:100,width:100,borderWidth:1,borderColor:'#666'}}
                    source={{
                    uri: `https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/${
                        data.name
                    }.png`,
                    }}
                />
                <Text >Name: {data.name}</Text>
                <Text >Height: {data.height}</Text>
                <Text >Weight: {data.weight}</Text>
                </View>
        )
    }
}

const mapStateToProps = (state) => ({
    pokemonDetailResponse: state.pokemon.pokemonDetailResponse,
    pokemonDetailError: state.pokemon.pokemonDetailError
})

const mapDispatchToProps = (dispatch) => ({
    pokemonDetailRequest: (param) => dispatch(PokemonActions.pokemonDetailRequest(param))
})

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(DetailScreen))