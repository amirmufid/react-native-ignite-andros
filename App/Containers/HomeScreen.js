import React, { Component } from 'react'
import { ScrollView, Text, Image, View, Button, FlatList, TouchableOpacity } from 'react-native'
import { Images } from '../Themes'
import { connect } from 'react-redux'
import PokemonActions from '../Redux/PokemonRedux' //here
import ListAvatar from '../Components/ListAvatar';

// Styles
import styles from './Styles/HomeScrenStyles'
import { withNavigation } from 'react-navigation'

class HomeScreen extends Component {
    constructor(){
        super()
        this.state={
          data:[],
          errorMessage: ''
        }
    }
    
    componentDidUpdate(prevProps){
        if(prevProps.pokemonResponse !== this.props.pokemonResponse){
          console.log('componentDidUpdate',this.props.pokemonResponse)
          this.setState({data: this.props.pokemonResponse})
        }
        if(prevProps.pokemonError !== this.props.pokemonError){
          this.setState({errorMessage: this.props.pokemonError})
        }
    }

    componentDidMount(){
        this.props.pokemonRequest()
    }

    goToDetailScreen = (item) => {
        this.props.navigation.navigate('DetailScreen', {name: item.name})
    }
    
    renderItem = ({item}) =>{
        return(
            <ListAvatar src={item.image} onPress={()=>{this.goToDetailScreen(item)}}>
                <Text style={styles.listTitle}>{item.name}</Text>
            </ListAvatar>
        )
    }

    render () {
        return (
            <View style={styles.mainContainer}>
                {this.state.errorMessage ? (
                    <Text>{this.state.errorMessage}</Text>
                ) : (
                    <FlatList 
                        data={this.state.data}
                        numColumns={1}
                        scrollEnabled={true}
                        renderItem={this.renderItem}
                        keyExtractor={item => item.name} 

                        renderItem={this.renderItem}
                    />
                )}
                
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    pokemonResponse: state.pokemon.pokemonResponse,
    pokemonError: state.pokemon.pokemonError
})

const mapDispatchToProps = (dispatch) => ({
    pokemonRequest: () => dispatch(PokemonActions.pokemonRequest())
})

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(HomeScreen))