import React, { Component } from 'react'
import { View, Button, FlatList, Image, Pressable } from 'react-native'
import { connect } from 'react-redux';

// Styles
import styles from './Styles/PageOneStyles'

import RoundedButton from '../Components/RoundedButton';

import faker from 'faker'

const data = [...Array(15).keys()].map( (_ , i ) => {
  return {
    key: faker.datatype.uuid(),
    color: faker.internet.color(),
  }
} );


 class PageOne extends Component {

  goToPage = () => {
    this.props.navigation.navigate('PageTwo')
  }

  buttonPress = (item) =>{
    alert(item.color)
  }

  render () {
    return (
        <View style={styles.mainContainer}>
            <Button
              title='Go To Page Two'
              onPress={this.goToPage}
            />
          <View style={styles.container}>
            <FlatList 
              data={data}
              keyExtractor={item=>item.key}
              renderItem={({item,index})=>{
                return (
                    <RoundedButton text={item.color} color={item.color} onPress={()=>{this.buttonPress(item)}} />
                )
              }}
            />
          </View>
        </View>
    )
  }
}

export default connect(null)(PageOne)