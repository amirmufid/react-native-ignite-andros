import React, { Component } from 'react'
import { View, Button, FlatList, Image, Pressable, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux';

// Styles
import styles from './Styles/PageThreeStyles'
import ListImage from '../Components/ListImage';

import faker from 'faker'

const data = [...Array(10).keys()].map( (_ , i ) => {
  return {
    key: faker.datatype.uuid(),
    image: faker.image.image()
  }
} );


 class PageThree extends Component {
    goToPage = () => {
        this.props.navigation.navigate('PageFour')
    }
    
    listOnPress = (item) =>{
        alert(item.image)
    }

    render () {
        return (
            <View style={styles.mainContainer}>
                <Button
                title='Go To Page Four'
                onPress={this.goToPage}
                />
            <View style={styles.container}>
                <FlatList 
                data={data}
                keyExtractor={item=>item.key}
                renderItem={({item,index})=>{
                    return (
                    <ListImage src={item.image} onPress={()=>{this.listOnPress(item)}} />
                    )
                }}
                />
            </View>
            </View>
        )
    }
}

export default connect(null)(PageThree)