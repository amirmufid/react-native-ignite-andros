import React, { Component } from 'react'
import { ScrollView, Text, Image, View } from 'react-native'
import { connect } from 'react-redux';
import { Images } from '../Themes'

import Button from '../Components/Button';

// Styles
import styles from './Styles/LaunchScreenStyles'

 class LaunchScreen extends Component {
  render () {
    return (
      <View style={styles.mainContainer}>
          <Button
            text='Hey there'
            onPress={() => window.alert('Full Button Pressed!')}
          />
      </View>
    )
  }
}

export default connect(null)(LaunchScreen)