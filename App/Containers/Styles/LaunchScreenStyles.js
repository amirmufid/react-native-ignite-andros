import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain'
  },
  centered: {
    alignItems: 'center'
  },
  button: {
    padding: 10,
    backgroundColor:'#000'
  },
  item: {
    padding: 20,
    fontSize: 18,
    height: 60,
    borderStyle: 'solid',
    borderColor: '#000',
    borderBottomWidth: 1,
    
    justifyContent: 'space-between',
    display: 'flex',
    flexGrow: 1,
    alignItems: 'center',
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },

})
