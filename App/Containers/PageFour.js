import React, { Component } from 'react'
import { View, Button, FlatList, Text, Alert } from 'react-native'
import { connect } from 'react-redux';

// Styles
import styles from './Styles/PageFourStyles'
import ListAvatar from '../Components/ListAvatar';

import faker from 'faker'

const data = [...Array(30).keys()].map( (_ , i ) => {
  return {
    key: faker.datatype.uuid(),
    image: faker.image.image(),
    name: faker.name.findName(),
    email: faker.internet.email()
  }
} );


 class PageThree extends Component {
    goToPage = () => {
        this.props.navigation.navigate('PageOne')
    }
    
    listOnPress = (item) =>{
        Alert.alert(
            item.name,
            item.email
          );
      
    }

    render () {
        return (
            <View style={styles.mainContainer}>
                <Button
                title='Back To Home'
                onPress={this.goToPage}
                />
            <View style={styles.container}>
                <FlatList 
                data={data}
                keyExtractor={item=>item.key}
                renderItem={({item,index})=>{
                    return (
                        <ListAvatar src={item.image} onPress={()=>{this.listOnPress(item)}}>
                            <Text style={styles.listTitle}>{item.name}</Text>
                            <Text style={styles.subtitle}>{item.email}</Text>
                        </ListAvatar>
                    )
                }}
                />
            </View>
            </View>
        )
    }
}

export default connect(null)(PageThree)