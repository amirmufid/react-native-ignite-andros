import React, { Component } from 'react'
import { View, Button, FlatList, Text, Pressable } from 'react-native'
import { connect } from 'react-redux';

// Styles
import styles from './Styles/PageTwoStyles'
import ListText from '../Components/ListText';

import faker from 'faker'

const data = [...Array(20).keys()].map( (_ , i ) => {
  return {
    key: faker.datatype.uuid(),
    name: faker.name.findName()
  }
} );


 class PageTwo extends Component {
  goToPage = () => {
    this.props.navigation.navigate('PageThree')
  }

  listOnPress = (item) => {
    alert(item.name)
  }

  render () {
    return (
        <View style={styles.mainContainer}>
            <Button
              title='Go To Page Three'
              onPress={this.goToPage}
            />
          <View style={styles.container}>
            <FlatList 
              data={data}
              keyExtractor={item=>item.key}
              renderItem={({item,index})=>{
                return (
                  <ListText key={item.key} text={item.name} onPress={()=>{this.listOnPress(item)}} />
                )
              }}
            />
          </View>
        </View>
    )
  }
}

export default connect(null)(PageTwo)