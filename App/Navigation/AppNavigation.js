
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack';
import PageOne from '../Containers/PageOne'
import PageTwo from '../Containers/PageTwo'
import PageThree from '../Containers/PageThree'
import PageFour from '../Containers/PageFour'
import HomeScreen from '../Containers/HomeScreen'
import DetailScreen from '../Containers/DetailScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  PageOne: { screen: PageOne,
    navigationOptions: ({navigation}) => ({
      title: 'Page One'
    })
  },
  PageTwo: { screen: PageTwo,
    navigationOptions: ({navigation}) => ({
      title: 'Page Two'
    })
  },
  PageThree: { screen: PageThree,
    navigationOptions: ({navigation}) => ({
      title: 'Page Three'
    })
  },
  PageFour: { screen: PageFour,
    navigationOptions: ({navigation}) => ({
      title: 'Page Four'
    })
  },
  HomeScreen: { screen: HomeScreen,
    navigationOptions: ({navigation}) => ({
      title: 'HomeScreen'
    })
  },
  DetailScreen: { screen: DetailScreen,
    navigationOptions: ({navigation}) => ({
      title: 'DetailScreen'
    })
  },
}, {
  // Default config for all screens
  headerMode: 'screen',
  initialRouteName: 'HomeScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default createAppContainer(PrimaryNav)
