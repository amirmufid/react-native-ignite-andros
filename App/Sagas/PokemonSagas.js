import { call, put } from 'redux-saga/effects'
import { path } from 'ramda'
import PokemonActions from '../Redux/PokemonRedux'

export function * getPokemon(api,action){
    const response = yield call(api.getPokemon)
    if (response.ok) {
        const data = path(['data','results'], response)
        data.map((obj,index)=>{
            obj.image = `https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/${obj.name}.png`;
            return obj
        })
        yield put(PokemonActions.pokemonSuccess(data))
    } else {
        const message = path(['data'], response)
        console.log(message,'error')
        yield put(PokemonActions.pokemonFailure(message))
    } 
}

export function * getDetailPokemon(api,action){
    const {name} = action;
    const response = yield call(api.getPokemonDetail,name)
    console.log(response)
    if(response.ok){
        const data = path(['data'], response)
        console.log(data,'success')
        yield put(PokemonActions.pokemonDetailSuccess(data))
    } else {
        const message = path(['data'], response)
        console.log(message,'error')
        yield put(PokemonActions.pokemonDetailFailure(message))
    } 
}