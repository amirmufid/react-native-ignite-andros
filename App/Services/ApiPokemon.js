import apisauce from 'apisauce'

const ApiPokemon = (baseURL = 'https://pokeapi.co/api/v2/') => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 10000
  })

  const getPokemon = () => api.get('pokemon')

  return {
    getPokemon
  }
}

export default ApiPokemon;