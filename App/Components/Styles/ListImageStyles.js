import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
    list: {
        marginTop:Metrics.baseMargin,
        paddingTop:Metrics.baseMargin,
        paddingBottom:Metrics.baseMargin
    },
    image: {
        width:'100%',
        height:100,
        borderRadius:5,
        resizeMode:'cover'
    }
})
