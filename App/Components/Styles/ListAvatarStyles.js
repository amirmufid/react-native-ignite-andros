import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
    list: {
        marginTop:Metrics.baseMargin,
        paddingTop:Metrics.baseMargin,
        paddingBottom:Metrics.baseMargin,
        flexDirection:'row',
        alignItems:'center'
    },
    avatar: {
        width:50,
        height:50,
        borderRadius:50,
        marginRight:10,
        resizeMode:'cover',
        borderWidth:1,
        borderColor:Colors.steel
    }
})
