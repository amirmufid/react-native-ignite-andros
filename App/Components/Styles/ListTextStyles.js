import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
    list: {
        marginTop:Metrics.baseMargin,
        paddingTop:Metrics.baseMargin,
        paddingBottom:Metrics.baseMargin,
        borderColor:Colors.steel,
        borderBottomWidth:1,
    },
    text: {
        fontSize:Fonts.size.h5
    }
})
