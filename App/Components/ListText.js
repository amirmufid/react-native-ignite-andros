import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, Text } from 'react-native'
import styles from './Styles/ListTextStyles'
import { StyleSheet } from 'react-native'


export default class ListText extends Component {
  static propTypes = {
    onPress: PropTypes.func,
    text: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.string
  }
  
  render () {
    let newStyle = StyleSheet.flatten([
        styles.list,
        this.props.style
    ]);

    return (
        <View style={newStyle} >
            <TouchableOpacity onPress={this.props.onPress}>
                <Text style={styles.text}>{this.props.text}</Text>
            </TouchableOpacity>
        </View>
    )
  }
}
