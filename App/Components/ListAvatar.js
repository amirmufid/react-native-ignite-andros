import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, Image, Text } from 'react-native'
import styles from './Styles/ListAvatarStyles'
import { StyleSheet } from 'react-native'


export default class ListAvatar extends Component {
  static propTypes = {
    onPress: PropTypes.func,
    src: PropTypes.string,
    style: PropTypes.object
  }
  
  render () {
    let newStyle = StyleSheet.flatten([
        styles.list,
        this.props.style
    ]);

    return (
        <TouchableOpacity onPress={this.props.onPress}>
            <View style={newStyle} >
                <Image source={{uri:this.props.src}} style={styles.avatar}/>
                <View>
                    {this.props.children}
                </View>
            </View>
        </TouchableOpacity>
    )
  }
}
