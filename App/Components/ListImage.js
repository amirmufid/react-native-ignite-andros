import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, Image } from 'react-native'
import styles from './Styles/ListImageStyles'
import { StyleSheet } from 'react-native'


export default class ListImage extends Component {
  static propTypes = {
    onPress: PropTypes.func,
    src: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.string
  }
  
  render () {
    let newStyle = StyleSheet.flatten([
        styles.list,
        this.props.style
    ]);

    return (
        <View style={newStyle} >
            <TouchableOpacity onPress={this.props.onPress}>
                <Image source={{uri:this.props.src}} style={styles.image}/>
            </TouchableOpacity>
        </View>
    )
  }
}
